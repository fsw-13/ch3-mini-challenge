function gradingStudents(grades) {
  let result = [];
  for(const grade in grades){
      let mod = grades[grade] % 5;
      if(mod >= 3 && grades[grade] >= 38)
          result[grade] = grades[grade] + (5 - mod)
      else
          result[grade] = grades[grade]
  }
  return result;
}